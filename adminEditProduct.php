<?php
require_once dirname(__FILE__) . '/adminAccess.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';

require_once dirname(__FILE__) . '/classes/Product.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';
$userId = $_SESSION['user_id'];

$conn = connDB();

$userDetails = getUser($conn, "WHERE user_id =?",array("user_id"),array($userId),"s");
$userData = $userDetails[0];

$userAmount = getUser($conn," WHERE user_type = 1 ");

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<!--<meta property="og:url" content="https://thousandmedia.asia/" />-->
<meta property="og:title" content="<?php echo _ADMIN_EDIT ?> <?php echo _INDEX_PRODUCT3 ?> | PPay" />
<title><?php echo _ADMIN_EDIT ?> <?php echo _INDEX_PRODUCT3 ?> | PPay</title>
<meta property="og:description" content="PPay" />
<meta name="description" content="PPay" />
<meta name="keywords" content="PPay">
<!--<link rel="canonical" href="https://thousandmedia.asia/" />-->
<?php include 'css.php'; ?>
</head>

<body class="body">

<?php include 'header.php'; ?>

<div class="width100 same-padding menu-distance2 admin-min-height-with-distance padding-bottom30">

  <?php
  if(isset($_POST['product_uid']))
  {
  $conn = connDB();
  $productDetails = getProduct($conn,"WHERE uid = ? ", array("uid") ,array($_POST['product_uid']),"s");
  ?>
    <h2 class="h1-title"><?php echo _ADMIN_EDIT ?> <?php echo $productDetails[0]->getName();?> <?php echo _ADMIN_DETAILS ?></h2> 

    <form action="utilities/updateProductFunction.php" method="POST" enctype="multipart/form-data">

      <div class="border-separation">
        <div class="dual-input">
          <p class="input-top-p admin-top-p"><?php echo _ADMIN_CATEGORY ?></p>
          <input class="input-name clean input-textarea admin-input" type="text" placeholder="<?php echo _ADMIN_CATEGORY ?>" value="<?php echo $productDetails[0]->getCategory();?>" name="update_category" id="update_category" required>
        </div>
        <div class="dual-input second-dual-input">
          <p class="input-top-p admin-top-p"><?php echo _ADMIN_BRAND ?></p>
          <input class="input-name clean input-textarea admin-input" type="text" placeholder="<?php echo _ADMIN_BRAND ?>" value="<?php echo $productDetails[0]->getBrand();?>" name="update_brand" id="update_brand" required>
        </div>

        <div class="clear"></div>

        <div class="dual-input">
          <p class="input-top-p admin-top-p"><?php echo _ADMIN_PRODUCT_NAME ?>*</p>
          <input class="input-name clean input-textarea admin-input" type="text" placeholder="<?php echo _ADMIN_PRODUCT_NAME ?>" value="<?php echo $productDetails[0]->getName();?>" name="update_product_name" id="update_product_name" required>
        </div>
        <div class="dual-input second-dual-input">
          <p class="input-top-p admin-top-p"><?php echo _ADMIN_PRICE ?></p>
          <input class="input-name clean input-textarea admin-input" type="text" placeholder="<?php echo _ADMIN_PRICE ?>" value="<?php echo $productDetails[0]->getPrice();?>" name="update_price" id="update_price" required>
        </div>

        <div class="clear"></div>

        <div class="dual-input">
          <p class="input-top-p admin-top-p"><?php echo _PRODUCT_DIAMOND ?></p>
          <input class="input-name clean input-textarea admin-input" type="text" placeholder="<?php echo _PRODUCT_DIAMOND ?>" value="<?php echo $productDetails[0]->getDiamond();?>" name="update_diamond" id="update_diamond" required>
        </div>

        <div class="clear"></div>

        <div class="width100 overflow">
          <p class="input-top-p admin-top-p"><?php echo _ADMIN_PRODUCT_DESC ?>* (<?php echo _ADMIN_AVOID_KEYIN ?> "'')</p>
          <textarea class="input-name clean input-textarea admin-input textare-real" type="text" placeholder="<?php echo _ADMIN_PRODUCT_DESC ?>" name="update_description" id="update_description" required><?php echo $productDetails[0]->getDescription();?></textarea>
        </div>

        <div class="clear"></div>

        <div class="width100 overflow margin-bottom10">
          <p class="input-top-p admin-top-p"><?php echo _ADMIN_UPLOAD_PRODUCT_PHOTO ?>: <a href="uploads/<?php echo $productDetails[0]->getImageOne();?>" class="blue-to-orange" target="_blank"><?php echo _ADMIN_VIEW_PRODUCT_PHOTO ?></a></p>
          <p><input id="file-upload" type="file" name="image_one" id="image_one" accept="image/*" class="margin-bottom10 pointer" /></p>
          <input class="aidex-input clean" type="hidden" value="<?php echo $productDetails[0]->getImageOne();?>" name="ori_fileone" id="ori_fileone"> 
        </div>

        <div class="clear"></div>

        <input class="aidex-input clean" type="hidden" value="<?php echo $productDetails[0]->getUid();?>" name="product_uid" id="product_uid"> 

        <div class="width100 overflow text-center">
          <button class="green-button white-text clean2 edit-1-btn margin-auto" name="submit"><?php echo _ADMIN_UPDATE ?></button>
        </div>

      </div>

    </form>
    
  <?php
  }
  ?>

</div>

<div class="clear"></div>

<?php include 'js.php'; ?>

</body>
</html>