<?php
if (session_id() == "")
{
  session_start();
}
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';
// require_once dirname(__FILE__) . '/sessionLoginChecker.php';

require_once dirname(__FILE__) . '/classes/Cart.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';
$uid = $_SESSION['user_id'];

$conn = connDB();

if($_SERVER['REQUEST_METHOD'] == 'POST'){
    addToCart();
    createOrder($conn,$uid);
    header('Location: ./checkout.php');
}

$userDetails = getUser($conn, "WHERE user_id =?",array("user_id"),array($uid),"s");
$userData = $userDetails[0];

// $slider = getSlider($conn," WHERE status = 'Show' ");
$products = getProduct($conn, "WHERE status = 'Available' ");

$productListHtml = "";

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
<?php include 'meta.php'; ?>
<!--<meta property="og:url" content="https://thousandmedia.asia/" />-->
<!--<link rel="canonical" href="https://thousandmedia.asia/" />-->
<meta property="og:title" content="PPay" />
<title>PPay</title>
<meta property="og:description" content="PPay" />
<meta name="description" content="PPay" />
<meta name="keywords" content="PPay,e-commerce,iphone,phone,huawei">

 <link rel="stylesheet" type="text/css" href="css/glider.css">
<?php include 'css.php'; ?>
</head>

<body class="body">
<?php include 'header.php'; ?>

<div class="width100 same-padding menu-distance">

    <div class="width100 scroll-div border-separation">
            <form method="POST">
                <?php
                    $conn = connDB();
                    if(isset($_SESSION['shoppingCart']) && $_SESSION['shoppingCart'])
                    {
                        unset($_SESSION['shoppingCart']);
                        echo " <h3> YOUR CART IS EMPTY </h3>";
                    }
                    else
                    {
                        echo " <h3> YOUR CART IS EMPTY </h3>";
                    }

                    $conn->close();
                ?>
            </form>
    </div>
    
</div>

<div class="clear"></div>

<?php include 'js.php'; ?>

</body>
</html>