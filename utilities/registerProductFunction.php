<?php
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';
require_once dirname(__FILE__) . '/../adminAccess.php';
require_once dirname(__FILE__) . '/../sessionLoginChecker.php';

require_once dirname(__FILE__) . '/../classes/Product.php';
require_once dirname(__FILE__) . '/../classes/User.php';

require_once dirname(__FILE__) . '/allNoticeModals.php';
require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';

$timestamp = time();

function registerNewProduct($conn,$uid,$category,$brand,$name,$price,$diamond,$description,$imageOne,$slug)
{
     if(insertDynamicData($conn,"product",array("uid","category","brand","name","price","diamond","description","image_one","slug"),
          array($uid,$category,$brand,$name,$price,$diamond,$description,$imageOne,$slug),"sssssssss") === null)
     {
          echo "gg";
     }
     else{    }
     return true;
}

if($_SERVER['REQUEST_METHOD'] == 'POST')
{
     $conn = connDB();

     $uid = md5(uniqid());

     $category= rewrite($_POST['register_category']);
     $brand = rewrite($_POST['register_brand']);
     $name = rewrite($_POST['register_name']);

     // $stringOne = ($_POST['register_brand']);
     // $brandType = preg_replace('/[^\p{L}\p{N}\s][^(\x20-\x7F)]*/u', '', $stringOne);
     // $updatedBrand = str_replace(' ', '-', trim($brandType));
     $stringTwo = ($_POST['register_name']);
     $productName = preg_replace('/[^\p{L}\p{N}\s][^(\x20-\x7F)]*/u', '', $stringTwo);
     $updatedProductName = str_replace(' ', '-', trim($productName));
     $slash = "-";
     // $slug = $updatedBrand.$slash.$updatedProductName;
     $slug = $updatedProductName;


     $price = rewrite($_POST['register_price']);
     $diamond = rewrite($_POST['register_diamond']);
     $description = rewrite($_POST['register_description']);

     $imageOne = $timestamp.$_FILES['image_one']['name'];
     $target_dir = "../uploads/";
     $target_file = $target_dir . basename($_FILES["image_one"]["name"]);
     // Select file type
     $imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));
     // Valid file extensions
     $extensions_arr = array("jpg","jpeg","png","gif");
     if( in_array($imageFileType,$extensions_arr) )
     {
          move_uploaded_file($_FILES['image_one']['tmp_name'],$target_dir.$imageOne);
     }

     $productNameRows = getProduct($conn," WHERE name = ? ",array("name"),array($_POST['register_name']),"s");
     $existingProductName = $productNameRows[0];

     if(!$existingProductName)
     {
          if(registerNewProduct($conn,$uid,$category,$brand,$name,$price,$diamond,$description,$imageOne,$slug))
          {
               // echo "product added";
               header('Location: ../adminAddProduct.php');
          }
          else
          {
               echo "fail";
          }
     }
     else
     {
          echo "Product name already used !!";
     }
}
else
{
     header('Location: ../index.php');
}
?>