<?php
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';
require_once dirname(__FILE__) . '/../sessionLoginChecker.php';

require_once dirname(__FILE__) . '/../classes/User.php';

require_once dirname(__FILE__) . '/allNoticeModals.php';
require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';

if($_SERVER['REQUEST_METHOD'] == 'POST')
{
    $conn = connDB();

    $userId = $_SESSION['user_id'];

    // $username = rewrite($_POST["update_username"]);
    $name = rewrite($_POST["update_name"]);

    $phone = rewrite($_POST["update_phone"]);
    $email = rewrite($_POST["update_email"]);
    $address = rewrite($_POST["update_address"]);

    // //   FOR DEBUGGING 
    // echo "<br>";
    // echo $uid."<br>";
    // echo $name."<br>";
    // echo $gender."<br>";
    // echo $dob."<br>";
    // echo $phone."<br>";
    // echo $email."<br>";

    $user = getUser($conn," user_id = ? ",array("user_id"),array($userId),"s");    

    if(!$user)
    {   
        $tableName = array();
        $tableValue =  array();
        $stringType =  "";
        //echo "save to database";

        // if($username)
        // {
        //     array_push($tableName,"username");
        //     array_push($tableValue,$username);
        //     $stringType .=  "s";
        // }

        if($name)
        {
            array_push($tableName,"name");
            array_push($tableValue,$name);
            $stringType .=  "s";
        }

        if($phone)
        {
            array_push($tableName,"phone");
            array_push($tableValue,$phone);
            $stringType .=  "s";
        }
        if($email)
        {
            array_push($tableName,"email");
            array_push($tableValue,$email);
            $stringType .=  "s";
        }
        if($address)
        {
            array_push($tableName,"address");
            array_push($tableValue,$address);
            $stringType .=  "s";
        }

        array_push($tableValue,$userId);
        $stringType .=  "s";
        $passwordUpdated = updateDynamicData($conn,"users"," WHERE user_id = ? ",$tableName,$tableValue,$stringType);
        if($passwordUpdated)
        {
            $_SESSION['messageType'] = 1;
            header('Location: ../profile.php?type=1');
        }
        else
        {
            $_SESSION['messageType'] = 1;
            header('Location: ../editProfile.php?type=2');
        }
    }
    else
    {
        $_SESSION['messageType'] = 1;
        header('Location: ../editProfile.php?type=3');
    }

}
else 
{
    header('Location: ../index.php');
}
?>
