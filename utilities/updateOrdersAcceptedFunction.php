<?php
require_once dirname(__FILE__) . '/../adminAccess.php';
require_once dirname(__FILE__) . '/../sessionLoginChecker.php';
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/../classes/Orders.php';
require_once dirname(__FILE__) . '/../classes/ProductOrders.php';
require_once dirname(__FILE__) . '/../classes/User.php';

require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';

if($_SERVER['REQUEST_METHOD'] == 'POST')
{
    $conn = connDB();

    $orderId = rewrite($_POST["order_id"]);

    $shipping_method = rewrite($_POST["shipping_method"]);
    $tracking_number = rewrite($_POST["tracking_number"]);
    // $shipping_status = rewrite($_POST["shipping_status"]);
    $delivered_on = rewrite($_POST["delivered_on"]);

    // $orderStatus = "Accepted";
    // $shipping_status = "Delivered";

    $orderStatus = "ACCEPTED";
    $shipping_status = "DELIVERED";

    // // for debugging
    // echo "<br>";
    // echo $orderId."<br>";
    // echo $shipping_method."<br>";
    // echo $tracking_number."<br>";
    // echo $delivered_on."<br>";
    // echo $orderStatus."<br>";
    // echo $shipping_status."<br>";

    if(isset($_POST['order_id']))
    {   
        $tableName = array();
        $tableValue =  array();
        $stringType =  "";
        //echo "save to database";
        if($shipping_method)
        {
            array_push($tableName,"shipping_method");
            array_push($tableValue,$shipping_method);
            $stringType .=  "s";
        }     
        if($tracking_number)
        {
            array_push($tableName,"tracking_number");
            array_push($tableValue,$tracking_number);
            $stringType .=  "s";
        }     
        if($orderStatus)
        {
            array_push($tableName,"payment_status");
            array_push($tableValue,$orderStatus);
            $stringType .=  "s";
        }
        if($shipping_status)
        {
            array_push($tableName,"shipping_status");
            array_push($tableValue,$shipping_status);
            $stringType .=  "s";
        } 
        if($delivered_on)
        {
            array_push($tableName,"shipping_date");
            array_push($tableValue,$delivered_on);
            $stringType .=  "s";
        }
        
        array_push($tableValue,$orderId);
        $stringType .=  "s";
        $orderUpdated = updateDynamicData($conn,"orders"," WHERE id = ? ",$tableName,$tableValue,$stringType);
        if($orderUpdated)
        {
            header('Location: ../adminViewApprovedOrders.php');
        }
        else
        {
            //echo "fail";
            $_SESSION['messageType'] = 1;
            header('Location: ../adminViewPendingOrders.php?type=2');
        }
    }
    else
    {
        //echo "dunno";
        $_SESSION['messageType'] = 1;
        header('Location: ../adminViewPendingOrders.php?type=3');
    }
}
else 
{
    header('Location: ../index.php');
}
?>