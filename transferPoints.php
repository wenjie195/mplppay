<?php
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';

require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';

$userId = $_SESSION['user_id'];

$conn = connDB();

$userDetails = getUser($conn, "WHERE user_id =?",array("user_id"),array($userId),"s");
$userData = $userDetails[0];

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
<?php include 'meta.php'; ?>
<!--<meta property="og:url" content="https://thousandmedia.asia/" />-->
<meta property="og:title" content="<?php echo _USER_CONVERT_POINTS_TO_DIAMONDS ?> | PPay" />
<title><?php echo _USER_CONVERT_POINTS_TO_DIAMONDS ?> | PPay</title>
<meta property="og:description" content="PPay" />
<meta name="description" content="PPay" />
<meta name="keywords" content="PPay">
<!--<link rel="canonical" href="https://thousandmedia.asia/" />-->
<?php include 'css.php'; ?>
</head>

<body class="body">

<?php include 'header.php'; ?>

<div class="width100 same-padding overflow min-height2 menu-distance text-center">
	<img src="img/diamond.png" class="convert-point-png" alt="<?php echo _USER_CONVERT_POINTS_TO_DIAMONDS ?>" title="<?php echo _USER_CONVERT_POINTS_TO_DIAMONDS ?>">
    <p class="review-product-name"><?php echo _USER_CONVERT_POINTS_TO_DIAMONDS ?></p>
    <p><?php echo _USER_CURRENT_POINTS ?>: <?php echo $userData->getPoint();?></>

    <!-- <form method="POST" action="utilities/adminICSearchFunction.php"> -->
    <form method="POST" action="utilities/convertPointsFunction.php">
      <div class="center-input2">
            
                <!-- <span class="input-span"><img src="img/coin.png" class="login-input-icon" alt="分数" title="分数"></span> -->
                <input class="input-name clean input-textarea" type="number" id="convert_amount" name="convert_amount" placeholder="<?php echo _USER_PLEASE_ENTER_AMOUNT ?>">
          
       </div> 
        <input class="login-input name-input clean" type="hidden" id="current_point" name="current_point" value="<?php echo $userData->getPoint();?>">
        <input class="login-input name-input clean" type="hidden" id="current_diamond" name="current_diamond" value="<?php echo $userData->getDiamond();?>">
        <div class="clear"></div>
        <button class="blue-gradient-button" type="submit"><?php echo _USER_SUBMIT ?></button>
        <div class="clear"></div>
            <div class="width100 overflow text-center go-back">
            	<a onclick="goBack()" class="green-a"><?php echo _USER_BACK ?></a>
            </div>
    </form>

</div>

<?php include 'js.php'; ?>

</body>
</html>