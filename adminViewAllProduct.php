<?php
require_once dirname(__FILE__) . '/adminAccess.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/Product.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';
$conn = connDB();

// $userDetails = getUser($conn," WHERE user_type = 1");
$products = getProduct($conn, "WHERE status = 'Available' ");

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

?>

<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<!--<meta property="og:url" content="https://thousandmedia.asia/" />-->
<meta property="og:title" content="<?php echo _PRODUCT_ALL ?> | PPay" />
<title><?php echo _PRODUCT_ALL ?> | PPay</title>
<meta property="og:description" content="PPay" />
<meta name="description" content="PPay" />
<meta name="keywords" content="PPay">
<!--<link rel="canonical" href="https://thousandmedia.asia/" />-->
<?php include 'css.php'; ?>
</head>

<body class="body">
<?php include 'header.php'; ?>
<div class="width100 same-padding menu-distance">
	<div class="width100">
        <div class="left-h1-div left-user-div">
            <h1 class="green-text h1-title"><?php echo _PRODUCT_ALL ?></h1>
            <div class="green-border"></div>
        </div>
    </div>

    <div class="right-add-div puppy-right-add-div">
        <a href="adminAddProduct.php"><div class="green-button white-text puppy-button"><?php echo _ADMIN_ADD_PRODUCT ?></div></a>
    </div>

    <div class="clear"></div>

	<div class="width100 scroll-div border-separation">
    	<table class="green-table width100">
        	<thead>
            	<tr>
                	<th><?php echo _ORDERS_NO ?></th>
                    <th><?php echo _ADMIN_PRODUCT_NAME ?></th>
                    <th><?php echo _PRODUCT_DIAMOND ?></th>
                    <th><?php echo _ADMIN_PRICE ?> (RM)</th>
                    <th><?php echo _ORDERS_ACTION ?></th>
                </tr>
            </thead>
            <tbody>
                <?php
                    if($products)
                    {
                        
                        for($cnt = 0;$cnt < count($products) ;$cnt++)
                        {?>
                            
                            <tr>
                                <td><?php echo ($cnt+1)?></td>
                                <td><?php echo $products[$cnt]->getName();?></td>
                                <td><?php echo $products[$cnt]->getDiamond();?></td>
                                <td><?php echo $products[$cnt]->getPrice();?></td>
                                <td>
                                    <form action="adminEditProduct.php" method="POST" class="hover1">
                                        <button class="clean blue-button2" type="submit" name="product_uid" value="<?php echo $products[$cnt]->getUid();?>">
                                            <?php echo _ADMIN_EDIT ?>
                                        </button>
                                    </form> 
                                </td>
                            </tr>
                            <?php
                        }
                    }
                ?>                                 
            </tbody>
        </table>
    </div>

    <div class="clear"></div>

    <div class="width100 bottom-spacing"></div>

</div>

<div class="clear"></div>

<?php include 'js.php'; ?>

<?php
if(isset($_GET['type']))
{
    $messageType = null;

    if($_SESSION['messageType'] == 1)
    {
        if($_GET['type'] == 1)
        {
            $messageType = "Product Update Successfully"; 
        }
        else if($_GET['type'] == 2)
        {
            $messageType = "Fail to update !!"; 
        }

        else if($_GET['type'] == 3)
        {
            $messageType = "ERROR !"; 
        }
        echo '
        <script>
            putNoticeJavascript("Notice !! ","'.$messageType.'");  
        </script>
        ';   
        $_SESSION['messageType'] = 0;
    }
}
?>

</body>
</html>