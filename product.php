<?php
if (session_id() == "")
{
  session_start();
}
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';
// require_once dirname(__FILE__) . '/sessionLoginChecker.php';

require_once dirname(__FILE__) . '/classes/Product.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';

$uid = $_SESSION['user_id'];

$conn = connDB();

if($_SERVER['REQUEST_METHOD'] == 'POST'){
    addToCart();
    header('Location: ./viewCart.php');
}

$userRows = getUser($conn," WHERE user_id = ? ",array("user_id"),array($_SESSION['user_id']),"s");
$userDetails = $userRows[0];

$products = getProduct($conn, "WHERE status = 'Available' ");

$productListHtml = "";

if(isset($_SESSION['shoppingCart']) && $_SESSION['shoppingCart']){
    $productListHtml = getShoppingCart($conn,1,true);
}else{
    if(isset($_POST['product-list-quantity-input'])){
        $productListHtml = createProductList($products,1,$_POST['product-list-quantity-input']);
    }else{
        $productListHtml = createProductList($products);
    }
}

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
<?php include 'meta.php'; ?>
<!--<meta property="og:url" content="https://thousandmedia.asia/" />-->
<!--<link rel="canonical" href="https://thousandmedia.asia/" />-->
<meta property="og:title" content="PPay" />
<title>PPay</title>
<meta property="og:description" content="PPay" />
<meta name="description" content="PPay" />
<meta name="keywords" content="PPay,e-commerce,iphone,phone,huawei">

<link rel="stylesheet" type="text/css" href="css/glider.css">
<?php include 'css.php'; ?>
</head>

<body class="body">
<?php include 'header.php'; ?>
                    
<div class="min-height overflow width100">

    <div class="width100 small-padding overflow min-height menu-distance2">
        <!-- <p class="review-product-name">All Products</p>    -->
        <p class="review-product-name"><?php echo _PRODUCT_ALL ?></p>  
        <div class="width103 product-big-div">
            <form method="POST">
                <?php echo $productListHtml; ?>

 
    				<div class="clear"></div>
                    <div class="width100 text-center stay-bottom-add"> 
                        <button class="clean black-button add-to-cart-btn green-button checkout-btn"><?php echo _ADMIN_ADD_TO_CART ?></button>
                    </div>    
          			<div class="stay-bottom-height"></div>
            </form>
        </div>
    </div>
    
</div>

<div class="clear"></div>
<div class="width100 same-padding green-footer cart-footer">
	<p class="footer-p white-text">© 2020 <?php echo _USER_PPAY_COPYRIGHT ?></p>
</div>
<style>
.green-footer{
	display:none;}
</style>
<script>
function goBack() {
  window.history.back();
}
</script>

<?php include 'js.php'; ?>

<script>
    $(".button-minus").on("click", function(e)
    {
        e.preventDefault();
        var $this = $(this);
        var $input = $this.closest("div").find("input");
        var value = parseInt($input.val());
        if (value > 1)
        {
            value = value - 1;
        } 
        else 
        {
            value = 0;
        }
        $input.val(value);
    });

    $(".button-plus").on("click", function(e)
    {
    e.preventDefault();
    var $this = $(this);
    var $input = $this.closest("div").find("input");
    var value = parseInt($input.val());
    if (value < 100)
    {
        value = value + 1;
    }
    else
    {
        value = 100;
    }
    $input.val(value);
    });
</script>

</body>
</html>