<?php
require_once dirname(__FILE__) . '/adminAccess.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';
// $conn = connDB();

// $userDetails = getUser($conn," WHERE user_type = 1");

// $conn->close();

$conn = connDB();

$userCnt = getUser($conn);

$userCount = count($userCnt);

if (isset($_GET['page']))
{
  $page = $_GET['page'];  // get page from link ?page=5
  $limitMax = 3500; // 50 user per page
  $startColumn = ($page - 1) * $limitMax; // start from? page = 2.. (2-1) * 50 = 50, start 50, LIMIT 50,50 start from 50 to next 50 user.
  $customerDetails = getUser($conn, "WHERE user_type = '1' LIMIT $startColumn,$limitMax ");
}
else 
{
  $limitMax = 3500;
  $startColumn = 0;
  $customerDetails = getUser($conn, "WHERE user_type = '1' LIMIT $startColumn,$limitMax ");
}

// $startColumn += 1; // for table no column
// $pagination = $userCount / $limitMax; // no of page

$startColumn += 0; // for table no column
$pagination = ($userCount / $limitMax) + 1 ; // no of page

// $conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

?>

<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<!--<meta property="og:url" content="https://thousandmedia.asia/" />-->
<meta property="og:title" content="<?php echo _ADMIN_ALL_USERS ?> | PPay" />
<title><?php echo _ADMIN_ALL_USERS ?> | PPay</title>
<meta property="og:description" content="PPay" />
<meta name="description" content="PPay" />
<meta name="keywords" content="PPay">
<!--<link rel="canonical" href="https://thousandmedia.asia/" />-->
<?php include 'css.php'; ?>
</head>

<body class="body">
<?php include 'header.php'; ?>
<div class="width100 same-padding menu-distance" id="myTable">
	<div class="width100">
   
            <h1 class="green-text h1-title"><?php echo _ADMIN_ALL_USERS ?></h1>
            <div class="green-border"></div>
     
    </div>

    <div class="clear"></div>
	<div class="width100 overflow section-margin">
   
      <div class="three-search-div">
        <p class="input-top-p"><?php echo _ADMIN_USER ?> ID</p>
        <div class="fake-input1">
        	<img src="img/search.png" class="search-input-png" alt="<?php echo _ADMIN_SEARCH ?>" title="<?php echo _ADMIN_SEARCH ?>">
        	<input type="text" id="myInput" onkeyup="myFunction()" placeholder="<?php echo _ADMIN_SEARCH ?> <?php echo _ADMIN_USER ?> ID" class="fake-input1-input clean">
        </div>
      </div>

      <div class="three-search-div mid-three-search-div">
        <p class="input-top-p"><?php echo _JS_USERNAME ?></p>
        <div class="fake-input1">
        	<img src="img/search.png" class="search-input-png" alt="<?php echo _ADMIN_SEARCH ?>" title="<?php echo _ADMIN_SEARCH ?>">
        	<input type="text" id="myInputB" onkeyup="myFunctionB()" placeholder="<?php echo _ADMIN_SEARCH ?> <?php echo _JS_USERNAME ?>" class="fake-input1-input clean">
        </div>
      </div>

      <div class="three-search-div">
        <p class="input-top-p"><?php echo _EDITPRO_NAME ?></p>
        <div class="fake-input1">
        	<img src="img/search.png" class="search-input-png" alt="<?php echo _ADMIN_SEARCH ?>" title="<?php echo _ADMIN_SEARCH ?>">
        	<input type="text" id="myInputC" onkeyup="myFunctionC()" placeholder="<?php echo _ADMIN_SEARCH ?> <?php echo _EDITPRO_NAME ?>" class="fake-input1-input clean">
        </div>
      </div>

	</div>
    <div class="clear"></div>

	<div class="width100 scroll-div">
    	<table class="green-table width100" id="myTable">
            <thead>
              <tr>
              <th><?php echo _ORDERS_NO ?></th>
                <th><?php echo _ADMIN_USER ?> ID</th>
                <th><?php echo _JS_USERNAME ?></th>
                <th><?php echo _EDITPRO_NAME ?></th>
                <th><?php echo PROFILE_PTS ?></th>
                <th><?php echo _PRODUCT_DIAMOND ?></th>
                <th><?php echo _ORDERS_CONTACT ?></th>
                <th><?php echo _ADMIN_EDIT ?></th>
              </tr>
            </thead>
            <tbody>
                <?php
                $conn = connDB();
                $customerDetails = getUser($conn, "WHERE user_type = '1' LIMIT $startColumn,$limitMax ");
                if($customerDetails)
                {   
                    for($cnt = 0;$cnt < count($customerDetails) ;$cnt++)
                    {
                    ?>
                        <tr>
                            <td><?php echo ($startColumn++)?></td>
                            <td><?php echo $customerDetails[$cnt]->getUserId();?></td>
                            <td><?php echo $customerDetails[$cnt]->getUsername();?></td>
                            <td><?php echo $customerDetails[$cnt]->getName();?></td>
                            <td><?php echo $customerDetails[$cnt]->getPoint();?></td>
                            <td><?php echo $customerDetails[$cnt]->getDiamond();?></td>
                            <td><?php echo $customerDetails[$cnt]->getPhone();?></td>
                            <td>
                                <form action="adminEditUser.php" method="POST" class="hover1">
                                    <button class="clean blue-button2" type="submit" name="user_id" value="<?php echo $customerDetails[$cnt]->getUserId();?>">
                                       <?php echo _ADMIN_EDIT ?>
                                    </button>
                                </form> 
                            </td>
                        </tr>
                    <?php
                    }
                    ?>
                <?php
                }
                $conn->close();
                ?>
            </tbody>
        </table>
    </div>

    <div class="clear"></div>

    <div class="pagination pagination-pop width100 text-center">
      <?php for ($i=1; $i < $pagination ; $i++)
      {
      ?>
        <?php if (isset($_GET['page']) && $_GET['page'] == $i) 
      {
      ?>
        <a class="active pagination-pop-a pagination-pop-a-active" href="<?php echo "adminAllUsers.php?page=".$i ?>"><?php echo $i ?></a>
      <?php
      }
      else
      {
      ?>
        <a class="pagination-pop-a" href="<?php echo "adminAllUsers.php?page=".$i ?>"><?php echo $i ?></a>
      <?php
      }
      ?>
      <?php
      }
      ?>
    </div>

    <!-- <div class="width100 bottom-spacing"></div> -->

</div>

<div class="clear"></div>

<?php include 'js.php'; ?>

<?php
if(isset($_GET['type']))
{
    $messageType = null;

    if($_SESSION['messageType'] == 1)
    {
        if($_GET['type'] == 1)
        {
            $messageType = "Update User Profile Successfully"; 
        }
        else if($_GET['type'] == 2)
        {
            $messageType = "Fail to update user profile";
        }
        else if($_GET['type'] == 3)
        {
            $messageType = "ERROR !!";
        }
        else if($_GET['type'] == 3)
        {
            $messageType = "Fail to record extra points !!";
        }
        echo '
        <script>
            putNoticeJavascript("Notice !! ","'.$messageType.'");  
        </script>
        ';   
        $_SESSION['messageType'] = 0;
    }
}
?>

<script>
function myFunction() {
  var input, filter, table, tr, td, i, txtValue;
  input = document.getElementById("myInput");
  filter = input.value.toUpperCase();
  table = document.getElementById("myTable");
  tr = table.getElementsByTagName("tr");
  for (i = 0; i < tr.length; i++) {
    td = tr[i].getElementsByTagName("td")[1];
    if (td) {
      txtValue = td.textContent || td.innerText;
      if (txtValue.toUpperCase().indexOf(filter) > -1) {
        tr[i].style.display = "";
      } else {
        tr[i].style.display = "none";
      }
    }       
  }
}
</script>

<script>
function myFunctionB() {
  var input, filter, table, tr, td, i, txtValue;
  input = document.getElementById("myInputB");
  filter = input.value.toUpperCase();
  table = document.getElementById("myTable");
  tr = table.getElementsByTagName("tr");
  for (i = 0; i < tr.length; i++) {
    td = tr[i].getElementsByTagName("td")[2];
    if (td) {
      txtValue = td.textContent || td.innerText;
      if (txtValue.toUpperCase().indexOf(filter) > -1) {
        tr[i].style.display = "";
      } else {
        tr[i].style.display = "none";
      }
    }       
  }
}
</script>

<script>
function myFunctionC() {
  var input, filter, table, tr, td, i, txtValue;
  input = document.getElementById("myInputC");
  filter = input.value.toUpperCase();
  table = document.getElementById("myTable");
  tr = table.getElementsByTagName("tr");
  for (i = 0; i < tr.length; i++) {
    td = tr[i].getElementsByTagName("td")[3];
    if (td) {
      txtValue = td.textContent || td.innerText;
      if (txtValue.toUpperCase().indexOf(filter) > -1) {
        tr[i].style.display = "";
      } else {
        tr[i].style.display = "none";
      }
    }       
  }
}
</script>

<!-- <script>
function myFunctionD() {
  var input, filter, table, tr, td, i, txtValue;
  input = document.getElementById("myInputD");
  filter = input.value.toUpperCase();
  table = document.getElementById("myTable");
  tr = table.getElementsByTagName("tr");
  for (i = 0; i < tr.length; i++) {
    td = tr[i].getElementsByTagName("td")[4];
    if (td) {
      txtValue = td.textContent || td.innerText;
      if (txtValue.toUpperCase().indexOf(filter) > -1) {
        tr[i].style.display = "";
      } else {
        tr[i].style.display = "none";
      }
    }       
  }
}
</script> -->

</body>
</html>