<?php
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/Orders.php';
require_once dirname(__FILE__) . '/classes/ProductOrders.php';
// require_once dirname(__FILE__) . '/classes/Product.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';

$conn = connDB();

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

?>

<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<!--<meta property="og:url" content="https://thousandmedia.asia/" />-->
<meta property="og:title" content="<?php echo _ORDERS_DETAILS ?>  | PPay" />
<title><?php echo _ORDERS_DETAILS ?> | PPay</title>
<meta property="og:description" content="PPay" />
<meta name="description" content="PPay" />
<meta name="keywords" content="PPay">
<!--<link rel="canonical" href="https://thousandmedia.asia/" />-->
<?php include 'css.php'; ?>
</head>

<body class="body">
<?php include 'header.php'; ?>

<div class="width100 same-padding menu-distance min-height2">

    <h1 class="green-text h1-title opacity-hover" onclick="goBack()">
        <img src="img/back-3.png" class="back-img">
            <?php echo _ORDERS_NUMBER ?>: #<?php echo $_POST['orderId'];?>
     
    </h1>

    <div class="clear"></div>

        <div class="width100 border-separation">
            <?php
            if(isset($_POST['orderId']))
            {
            $orderId = $_POST['orderId'];
            $conn = connDB();
            $orders = getProductOrders($conn,"WHERE order_id = ? ", array("order_id") ,array($_POST['orderId']),"i");
            // $orders = getProductOrders($conn);
            $order = getOrders($conn);
            ?>
                <div class="width100 overflow">
                    <p class="green-text top-text"><?php echo _ORDERS_DETAILS ?></p>
                    <div class="table-scroll-div">
                        <table class="order-table">
                            <thead>	
                                <tr>
                                    <th><?php echo _ORDERS_NO ?></th>
                                    <th><?php echo _INDEX_PRODUCT ?></th>
                                    <th><?php echo _ORDERS_QTY ?></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php 
                                for($cnt = 0;$cnt < count($orders) ;$cnt++)
                                {
                                    if($orders[$cnt]->getOrderId() == $orderId)
                                    {
                                    ?>
                                        <tr>
                                            <td><?php echo $cnt+1;?></td>
                                            <td><?php echo $orders[$cnt]->getProductName();?></td>
                                            <td><?php echo $orders[$cnt]->getQuantity();?></td>
                                        </tr>
                                    <?php
                                    }
                                }
                                ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            <?php
            }
            ?>

            <?php
            if(isset($_POST['orderId']))
            {
            $conn = connDB();
            $orders = getOrders($conn,"WHERE id = ? ", array("id") ,array($_POST['orderId']),"s");
            ?>
                <div class="width100 overflow some-margin-bottom margin-top30">
                    <p class="green-text top-text"><?php echo _ORDERS_DELIVERY_DETAILS ?></p>
                    <p class="bottom-text"><?php echo _EDITPRO_NAME ?>: <?php echo $orders[0]->getName();?></p>  
                    <p class="bottom-text"><?php echo _ORDERS_CONTACT ?>: <?php echo $orders[0]->getContactNo();?></p>
                    <p class="bottom-text"><?php echo _ORDERS_ADDRESS ?>:<?php echo $orders[0]->getAddressLine1();?></p>
                </div>
            <?php
            }
            ?>

        </div>

        <div class="dual-div">
            <p class="top-text"><?php echo _ORDERS_STATUS ?>: <?php echo $orders[0]->getShippingStatus();?></p>
        </div>

        <div class="dual-div second-dual-div">
            <p class="top-text"><?php echo _ORDERS_DATE ?>: <?php echo $orders[0]->getDateUpdated();?></p>
        </div> 

        <div class="clear"></div>

        <?php 
            $payStatus = $orders[0]->getPaymentStatus();
            $shipStatus = $orders[0]->getShippingStatus();
            if($payStatus == "ACCEPTED" && $shipStatus == "DELIVERED")
            {
            ?>
            
                <div class="dual-div">
                    <p class="top-text"><?php echo _ORDERS_DATE ?>: <?php echo $orders[0]->getShippingMethod();?></p>
                </div>

                <div class="dual-div second-dual-div">
                    <p class="top-text"><?php echo _ORDERS_TRACKING_NUMBER ?>: <?php echo $orders[0]->getTrackingNumber();?></p>
                </div>       
        
            <?php
            }
            else{}
        ?>

    <div class="clear"></div>

    

</div>

<div class="clear"></div>

<?php include 'js.php'; ?>

<script>
function goBack() 
{
    window.history.back();
}
</script>

</body>
</html>