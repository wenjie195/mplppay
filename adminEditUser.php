<?php
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';

require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';
$userId = $_SESSION['user_id'];

$conn = connDB();

// $userDetails = getUser($conn, "WHERE user_id =?",array("user_id"),array($userId),"s");
// $userData = $userDetails[0];

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!doctype html>
<html>

<head>
<?php include 'meta.php'; ?>
<!--<meta property="og:url" content="https://thousandmedia.asia/" />-->
<meta property="og:title" content="<?php echo _EDITPRO_TITLE ?> | PPay" />
<title><?php echo _EDITPRO_TITLE ?> | PPay</title>
<meta property="og:description" content="PPay" />
<meta name="description" content="PPay" />
<meta name="keywords" content="PPay">
<!--<link rel="canonical" href="https://thousandmedia.asia/" />-->
<?php include 'css.php'; ?>
</head>

<body class="body">
<?php include 'header.php'; ?>
	<div class="width100 same-padding overflow min-height2 menu-distance">
        <p class="review-product-name"><?php echo _EDITPRO_TITLE ?></p>
        
        <?php
        if(isset($_POST['user_id']))
        {
        $conn = connDB();
        $userDetails = getUser($conn,"WHERE user_id = ? ", array("user_id") ,array($_POST['user_id']),"s");
        ?>

            <form method="POST" action="utilities/adminEditProfileFunction.php">

                <div class="dual-input">
                    <p class="input-top-p"><?php echo _EDITPRO_NAME ?></p>
                    <input class="input-name clean input-textarea" type="text" placeholder="<?php echo _EDITPRO_NAME ?>" value="<?php echo $userDetails[0]->getName();?>" name="update_name" id="update_name" required>   
                </div>

                <div class="dual-input second-dual-input">
                    <p class="input-top-p"><?php echo _JS_USERNAME ?></p>
                    <input class="input-name clean input-textarea" type="text" placeholder="<?php echo _JS_USERNAME ?>" value="<?php echo $userDetails[0]->getUsername();?>" name="update_username" id="update_username" required> 
                </div>

                <div class="clear"></div>

                <div class="dual-input">
                    <p class="input-top-p"><?php echo _EDITPRO_PHONE ?></p>
                    <input class="input-name clean" type="text" placeholder="<?php echo _EDITPRO_PHONE ?>" value="<?php echo $userDetails[0]->getPhone();?>" id="update_phone" name="update_phone">  
                </div>

                <div class="dual-input second-dual-input">
                    <p class="input-top-p"><?php echo _EDITPRO_EMAIL ?></p>
                    <input class="input-name clean" type="email" placeholder="<?php echo _EDITPRO_EMAIL ?>" value="<?php echo $userDetails[0]->getEmail();?>" id="update_email" name="update_email"> 
                </div>

                <div class="clear"></div>

                <div class="dual-input">
                    <p class="input-top-p"><?php echo _USER_CURRENT_POINTS ?></p>
                    <input class="input-name clean" type="text" value="<?php echo $userDetails[0]->getPoint();?>" id="current_points" name="current_points" readonly>  
                </div>

                <div class="dual-input second-dual-input">
                    <p class="input-top-p"><?php echo _ADMIN_EXTRA_POINTS ?></p>
                    <input class="input-name clean" type="text" placeholder="<?php echo _ADMIN_EXTRA_POINTS ?>" id="extra_points" name="extra_points"> 
                </div>

                <input class="input-name clean" type="hidden" value="<?php echo $userDetails[0]->getUserId();?>" id="user_id" name="user_id" readonly> 

                <div class="clear"></div>

                <div class="width100 overflow text-center">     
                    <button class="green-button white-text clean2 edit-1-btn margin-auto" name="submit"><?php echo _USER_SUBMIT ?></button>
                </div>

            </form>

        <?php
        }
        ?>

    </div>
    
<div class="clear"></div>

<?php include 'js.php'; ?>

<?php
if(isset($_GET['type']))
{
    $messageType = null;

    if($_SESSION['messageType'] == 1)
    {
        if($_GET['type'] == 1)
        {
            $messageType = "Update Profile Successfully"; 
        }
        else if($_GET['type'] == 2)
        {
            $messageType = "Fail to update profile";
        }
        else if($_GET['type'] == 3)
        {
            // $messageType = "User details is not available !!";
            $messageType = "ERROR !!";
        }
        echo '
        <script>
            putNoticeJavascript("Notice !! ","'.$messageType.'");  
        </script>
        ';   
        $_SESSION['messageType'] = 0;
    }
}
?>

</body>
</html>