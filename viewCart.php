<?php
if (session_id() == "")
{
  session_start();
}
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';
// require_once dirname(__FILE__) . '/sessionLoginChecker.php';

require_once dirname(__FILE__) . '/classes/Cart.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';

$uid = $_SESSION['user_id'];

$conn = connDB();

if($_SERVER['REQUEST_METHOD'] == 'POST'){
    addToCart();
    createOrder($conn,$uid);
    header('Location: ./checkout.php');
}

$userDetails = getUser($conn, "WHERE user_id =?",array("user_id"),array($uid),"s");
$userData = $userDetails[0];

// $slider = getSlider($conn," WHERE status = 'Show' ");
$products = getProduct($conn, "WHERE status = 'Available' ");

$productListHtml = "";

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
<?php include 'meta.php'; ?>
<!--<meta property="og:url" content="https://thousandmedia.asia/" />-->
<!--<link rel="canonical" href="https://thousandmedia.asia/" />-->
<meta property="og:title" content="<?php echo _ADMIN_VIEW_CART ?> | PPay" />
<title><?php echo _ADMIN_VIEW_CART ?> | PPay</title>
<meta property="og:description" content="PPay" />
<meta name="description" content="PPay" />
<meta name="keywords" content="PPay,e-commerce,iphone,phone,huawei">

 <link rel="stylesheet" type="text/css" href="css/glider.css">
<?php include 'css.php'; ?>
</head>

<body class="body">
<?php include 'header.php'; ?>

<div class="width100 same-padding min-height adjust-padding">
    </br></br></br>
    <a href="cartEmpty.php">
        <button class="right-delete clean transparent-button"><?php echo _ADMIN_DELETE_ALL ?></button>
        <div class="clear"></div>
    </a>

	<div  id="Cart" class="block same-padding">
        <form method="POST">
            <?php
                $conn = connDB();
                if(isset($_SESSION['shoppingCart']) && $_SESSION['shoppingCart'])
                {
                    $productListHtml = getShoppingCart($conn,2);
                    echo $productListHtml;
                }
                else
                {
                    echo " <h3> YOUR CART IS EMPTY </h3>";
                }


                if(array_key_exists('xclearCart', $_POST))
                {
                    xclearCart();
                }
                else
                {
                // code...
                    unset($productListHtml);
                }

                $conn->close();
            ?>
        </form>
    </div>  
</div>

<style>
.green-footer{
	display:none;}
</style>
<div class="clear"></div>
<div class="width100 same-padding green-footer cart-footer">
	<p class="footer-p white-text">© 2020 <?php echo _USER_PPAY_COPYRIGHT ?></p>
</div>
<?php include 'js.php'; ?>

</body>
</html>
