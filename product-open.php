<?php
if (session_id() == "")
{
  session_start();
}
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';
// require_once dirname(__FILE__) . '/sessionLoginChecker.php';

require_once dirname(__FILE__) . '/classes/Product.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';

$conn = connDB();

// $slider = getSlider($conn," WHERE status = 'Show' ");
$products = getProduct($conn, "WHERE status = 'Available' ");

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
<?php include 'meta.php'; ?>
<!--<meta property="og:url" content="https://thousandmedia.asia/" />-->
<!--<link rel="canonical" href="https://thousandmedia.asia/" />-->
<meta property="og:title" content="Products | PPay" />
<title>Products | PPay</title>
<meta property="og:description" content="PPay" />
<meta name="description" content="PPay" />
<meta name="keywords" content="PPay,e-commerce,iphone,phone,huawei">

 <link rel="stylesheet" type="text/css" href="css/glider.css">
<?php include 'css.php'; ?>
</head>

<body class="body">
<?php include 'header.php'; ?>

<div class="min-height overflow width100">

    <div class="width100 small-padding overflow min-height menu-distance2">
        <!-- <p class="review-product-name">All Products</p>   -->
        <h1 class="green-text user-title left-align-title"><?php echo _PRODUCT_ALL ?></h1>  
        <div class="clear"></div>
        <div class="width103 product-big-div">
            <?php
            $conn = connDB();
            if($products)
            {
                for($cnt = 0;$cnt < count($products) ;$cnt++)
                {
                ?>
                    <a href='productDetails-open.php?id=<?php echo $products[$cnt]->getUid();?>' target="_blank">
                        <div class="shadow-white-box four-box-size ow-product-big-div opacity-hover">
                         <div class="square2">
                            <div class="width100 white-bg content2">
                                <img src="uploads/<?php echo $products[$cnt]->getImageOne();?>" alt="<?php echo $products[$cnt]->getName();?>" title="<?php echo $products[$cnt]->getName();?>" class="width100 two-border-radius">
                            </div>
                          </div>
                            <div class="width100 product-details-div">
                                <p class="width100 text-overflow slider-product-name"><?php echo $products[$cnt]->getName();?></p>
                                <p class="slider-product-price text-overflow text-center">RM <?php echo $products[$cnt]->getPrice();?> / <?php echo $products[$cnt]->getDiamond();?> <?php echo _PRODUCT_DIAMOND ?></p>
                                <div class="clear"></div>
                            </div>
                        </div>
                    </a>
                <?php
                }
                ?>
            <?php
            }
            $conn->close();
            ?>
        </div>
    </div>

</div>

<div class="clear"></div>

<?php include 'js.php'; ?>

</body>
</html>