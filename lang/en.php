<?php
//apply in all
define("_MAINJS_INDEX_LOGIN", "Login");
define("_MAINJS_ALL_LOGOUT", "Logout");
define("_MAINJS_INDEX_USERNAME", "Username");
define("_MAINJS_INDEX_PASSWORD", "Password");
//JS
define("_JS_USERNAME", "Username");
define("_JS_PASSWORD", "Password");
define("_JS_VIEW_PASSWORD", "View Password");
//index and header
define("_INDEX_HOME", "Home");
define("_INDEX_PRODUCT", "Products");
define("_INDEX_PRODUCT3", "Product");
define("_INDEX_PRODUCT2", "Hot Selling");
define("_INDEX_PROFILE", "Profile");
define("_INDEX_ORDER", "Orders");
define("_INDEX_PENDING", "Pending Orders");
define("_INDEX_APPROVED", "Approved Orders");
define("_INDEX_REJECTED", "Rejected Orders");
define("_INDEX_VIEW_MORE", "View More");
//product / all product
define("_PRODUCT_ALL", "All Products");
//product
define("_PRODUCT_DIAMOND", "Diamonds");
//profile
define("PROFILE_CURRENT_PTS", "Current Points");
define("PROFILE_DIAMOND", "Current Diamonds");
define("PROFILE_PTS_DIAMOND", "Points to Diamonds");
define("PROFILE_PTS", "Points");
define("PROFILE_HI", "Hi");
//edit profile
define("_EDITPRO_TITLE", "Edit Profile");
define("_EDITPRO_NAME", "Name");
define("_EDITPRO_PHONE", "Phone No.");
define("_EDITPRO_EMAIL", "Email");
define("_EDITPRO_ADDRESS", "Address");
define("_EDITPRO_SUBMIT", "Submit");
//orders (pending,approved, rejected)
define("_ORDERS_PENDING", "Pending Orders");
define("_ORDERS_APPROVED", "Approved Orders");
define("_ORDERS_REJECTED", "Rejected Orders");
define("_ORDERS_NO", "No.");
define("_ORDERS_ORDERID", "Order ID");
define("_ORDERS_USERNAME", "Username");
define("_ORDERS_CONTACT", "Contact");
define("_ORDERS_ADDRESS", "Address");
define("_ORDERS_DATE", "Date");
define("_ORDERS_ACTION", "Action");
define("_ORDERS_VIEW", "View");
define("_ORDERS_NUMBER", "Order Number");
define("_ORDERS_DETAILS", "Order Details");
define("_ORDERS_QTY", "Quantity");
define("_ORDERS_DELIVERY_DETAILS", "Delivery Details");
define("_ORDERS_STATUS", "Order Status");
define("_ORDERS_SHIPPING_METHOD", "Shipping Method");
define("_ORDERS_TRACKING_NUMBER", "Tracking Number");
//User
define("_USER_SUBMIT", "Submit");
define("_USER_CONVERT_POINTS_TO_DIAMONDS", "Convert Points to Diamonds");
define("_USER_PLEASE_ENTER_AMOUNT", "Please Enter the Amount of Points to Transfer");
define("_USER_CURRENT_POINTS", "Current Points");
define("_USER_PPAY_COPYRIGHT", "PPay, All Rights Reserved.");
define("_USER_EMAIL", "Email");
define("_USER_COUNTRY", "Country");
define("_USER_CHOOSE_A_COUNTRY", "Please choose a country");
define("_USER_BACK", "Back");
define("_USER_DELIVER_TO", "Deliver to");
define("_USER_ADDRESS_1", "Address Line 1");
define("_USER_TOTAL_DIAMONDS_DEDUCT", "Total Diamonds Need to Deduct");
define("_USER_AVAILABLE_DIAMONDS", "Available Diamonds");
define("_USER_INSUFFICIENT_DIAMONDS", "Insufficient diamonds to pay. Please convert your points to diamonds.");
//Admin
define("_ADMIN_DASHBOARD", "Dashboard");
define("_ADMIN_ALL_USERS", "All Users");
define("_ADMIN_ADD_NEW_USER", "Add New User");
define("_ADMIN_TOP_UP_USER_POINTS", "Top Up User Points");
define("_ADMIN_RENEW_USER_POINTS", "Renew User Points (Direct Renew)");
define("_PRODUCT_POINTS_CONVERSION", "Points Conversion");
define("_ADMIN_USER", "User");
define("_ADMIN_TOTAL_USERS", "Total User");
define("_ADMIN_TOTAL_PRODUCT", "Total Products");
define("_ADMIN_SEARCH", "Search");
define("_ADMIN_EDIT", "Edit");
define("_ADMIN_EXTRA_POINTS", "Extra Points");
define("_ADMIN_ADD_PRODUCT", "Add Product");
define("_ADMIN_PRODUCT_NAME", "Product Name");
define("_ADMIN_PRICE", "Price");
define("_ADMIN_DETAILS", "Details");
define("_ADMIN_CATEGORY", "Category");
define("_ADMIN_BRAND", "Brand");
define("_ADMIN_PRODUCT_NAME2", "Product Name");
define("_ADMIN_PRODUCT_DESC", "Product Description");
define("_ADMIN_AVOID_KEYIN", "Avoid Key In");
define("_ADMIN_UPLOAD_PRODUCT_PHOTO", "Upload Product Photo");
define("_ADMIN_VIEW_PRODUCT_PHOTO", "Click To View the Old Product Image");
define("_ADMIN_UPDATE", "Update");
define("_ADMIN_IMPORT_EXCEL", "Import Excel");
define("_ADMIN_ADD_ON_USER_POINTS", "Add On User Points");
define("_ADMIN_RENEW_USER_POINTSS", "Renew User Points");
define("_ADMIN_ALL_POINTS", "All Points Conversion");
define("_ADMIN_POINTS_CONVERT", "Points Convert");
define("_ADMIN_TOTAL_DIAMONDS", "Total Diamonds");
define("_ADMIN_QUANTITY", "Quantity");
define("_ADMIN_ISSUE_DATE", "Issue Date");
define("_ADMIN_DELIVERY_ADDRESS", "Delivery Address");
define("_ADMIN_ADD_TO_CART", "Add To Cart");
define("_ADMIN_VIEW_CART", "View Cart");
define("_ADMIN_DELETE_ALL", "Delete All");
define("_USER_COMPLETE_ORDER", "Complete Order");
define("_ADMIN_ADD_NEW_CATEGORY", "Add New Categoty Here");
define("_ADMIN_NEXT", "Next");
define("_ADMIN_ADD_NEW_CATEGORY2", "Add New Category");
define("_ADMIN_CATEGORY_NAME", "Category Name");