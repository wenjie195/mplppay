<?php
require_once dirname(__FILE__) . '/adminAccess.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

// require_once dirname(__FILE__) . '/classes/Product.php';
require_once dirname(__FILE__) . '/classes/ConvertPointReport.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';
$conn = connDB();

$products = getConvertPointReport($conn);
// $products = getConvertPointReport($conn, "WHERE status = 'Available' ");

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

?>

<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<!--<meta property="og:url" content="https://thousandmedia.asia/" />-->
<meta property="og:title" content="<?php echo _ADMIN_ALL_POINTS ?> | PPay" />
<title><?php echo _ADMIN_ALL_POINTS ?> | PPay</title>
<meta property="og:description" content="PPay" />
<meta name="description" content="PPay" />
<meta name="keywords" content="PPay">
<!--<link rel="canonical" href="https://thousandmedia.asia/" />-->
<?php include 'css.php'; ?>
</head>

<body class="body">
<?php include 'header.php'; ?>
<div class="width100 same-padding menu-distance2 admin-min-height-with-distance">
	<div class="width100">
        
            <h1 class="green-text h1-title"><?php echo _ADMIN_ALL_POINTS ?></h1>
            <div class="green-border"></div>
      
    </div>

    <div class="clear"></div>

	<div class="width100 overflow section-margin">
   
       
        <div class="fake-input1">
        	<img src="img/search.png" class="search-input-png" alt="<?php echo _ADMIN_SEARCH ?>" title="<?php echo _ADMIN_SEARCH ?>">
        	<input type="text" id="myInput" onkeyup="myFunction()" placeholder="<?php echo _ADMIN_SEARCH ?> <?php echo _MAINJS_INDEX_USERNAME ?>" class="fake-input1-input clean">
        </div>
 

	</div>

	<div class="width100 scroll-div">
    	<table class="green-table width100" id="myTable">
        	<thead>
            	<tr>
                	<th><?php echo _ORDERS_NO ?></th>
                    <th><?php echo _ORDERS_USERNAME ?></th>
                    <th><?php echo _ADMIN_POINTS_CONVERT ?></th>
                    <th><?php echo _ADMIN_TOTAL_DIAMONDS ?></th>
                    <!-- <th>Date</th> -->
                </tr>
            </thead>
            <tbody>
                <?php
                    if($products)
                    {
                        
                        for($cnt = 0;$cnt < count($products) ;$cnt++)
                        {?>
                            
                            <tr>
                                <td><?php echo ($cnt+1)?></td>
                                <td><?php echo $products[$cnt]->getUsername();?></td>
                                <td><?php echo $products[$cnt]->getConvertPoints();?></td>
                                <td><?php echo $products[$cnt]->getDiamond();?></td>
                                <!-- <td><?php //echo date("d-m-Y",strtotime($products[$cnt]->getDateCreated()));?></td> -->
                            </tr>
                            <?php
                        }
                    }
                ?>                                 
            </tbody>
        </table>
    </div>

    <div class="clear"></div>

    <div class="width100 bottom-spacing"></div>

</div>

<div class="clear"></div>

<?php include 'js.php'; ?>

<script>
function myFunction() {
  var input, filter, table, tr, td, i, txtValue;
  input = document.getElementById("myInput");
  filter = input.value.toUpperCase();
  table = document.getElementById("myTable");
  tr = table.getElementsByTagName("tr");
  for (i = 0; i < tr.length; i++) {
    td = tr[i].getElementsByTagName("td")[1];
    if (td) {
      txtValue = td.textContent || td.innerText;
      if (txtValue.toUpperCase().indexOf(filter) > -1) {
        tr[i].style.display = "";
      } else {
        tr[i].style.display = "none";
      }
    }       
  }
}
</script>

<?php
if(isset($_GET['type']))
{
    $messageType = null;

    if($_SESSION['messageType'] == 1)
    {
        if($_GET['type'] == 1)
        {
            $messageType = "Product Update Successfully"; 
        }
        else if($_GET['type'] == 2)
        {
            $messageType = "Fail to update !!"; 
        }

        else if($_GET['type'] == 3)
        {
            $messageType = "ERROR !"; 
        }
        echo '
        <script>
            putNoticeJavascript("Notice !! ","'.$messageType.'");  
        </script>
        ';   
        $_SESSION['messageType'] = 0;
    }
}
?>

</body>
</html>