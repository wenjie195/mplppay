<?php
// if (session_id() == "")
// {
//   session_start();
// }
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';

require_once dirname(__FILE__) . '/classes/Product.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';
$conn = connDB();

// $slider = getSlider($conn," WHERE status = 'Show' ");
$products = getProduct($conn, "WHERE status = 'Available' ");

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
<?php include 'meta.php'; ?>
<!--<meta property="og:url" content="https://thousandmedia.asia/" />-->
<!--<link rel="canonical" href="https://thousandmedia.asia/" />-->
<meta property="og:title" content="PPay" />
<title>PPay</title>
<meta property="og:description" content="PPay" />
<meta name="description" content="PPay" />
<meta name="keywords" content="PPay,e-commerce,iphone,phone,huawei">

<link rel="stylesheet" type="text/css" href="css/glider.css">
<?php include 'css.php'; ?>
</head>

<body class="body">
<?php include 'header.php'; ?>

<?php 
    // Program to display URL of current page. 
    if(isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on') 
    $link = "https"; 
    else
    $link = "http"; 

    // Here append the common URL characters. 
    $link .= "://"; 

    // Append the host(domain name, ip) to the URL. 
    $link .= $_SERVER['HTTP_HOST']; 

    // Append the requested resource location to the URL 
    $link .= $_SERVER['REQUEST_URI']; 

    if(isset($_GET['id']))
    {
        $referrerUidLink = $_GET['id'];
    }
    else 
    {
        $referrerUidLink = "";
    }
?>

<div class="width100 same-padding  menu-distance2 admin-min-height-with-distance">

    <?php
    if(isset($_GET['id']))
    {
    $conn = connDB();
    $productDetails = getProduct($conn,"WHERE uid = ? ", array("uid") ,array($_GET['id']),"s");
    // $productDetails = getProduct($conn,"WHERE slug = ? ", array("slug") ,array($_GET['id']),"s");
    ?>
    <?php
    }
    ?>

	<div class="left-image-div">
        <?php
        if($productDetails)
        {
        for($cnt = 0;$cnt < count($productDetails) ;$cnt++)
        {
        ?>

        <div class="item">            
            <div class="clearfix">
                <ul id="image-gallery" class="gallery list-unstyled cS-hidden">
                    <li data-thumb="uploads/<?php echo $productDetails[$cnt]->getImageOne();?>" class="pet-slider-li"> 
                        <img src="uploads/<?php echo $productDetails[$cnt]->getImageOne();?>" class="pet-slider-img" alt="<?php echo $productDetails[$cnt]->getName();?>" title="<?php echo $productDetails[$cnt]->getName();?>"/>
                    </li>
                </ul>
            </div>
        </div>

        <?php
        }
        ?>
        <?php
        }
        ?>
    </div>
    
    <div class="right-content-div2">
        <?php
        if($productDetails)
        {
        for($cnt = 0;$cnt < count($productDetails) ;$cnt++)
        {
        ?>
            <p class="green-text breed-p"><?php echo $productDetails[$cnt]->getBrand();?></p>
            <h1 class="green-text pet-name"><?php echo $productDetails[$cnt]->getName();?></h1>

            <div class="clear"></div>
            
            <?php
            if($productDetails)
            {
            for($cnt = 0;$cnt < count($productDetails) ;$cnt++)
            {
            ?>
                <div class="pet-details-div">
                    <table class="pet-table">
                        <tr>
                            <td class="grey-p">Category</td>
                            <td class="grey-p">:</td>
                            <td><?php echo $productDetails[$cnt]->getCategory();?></td>
                        </tr>
                        <tr>
                            <td class="grey-p">Brand</td>
                            <td class="grey-p">:</td>
                            <td><?php echo $productDetails[$cnt]->getBrand();?></td>
                        </tr>  
                        <tr>
                            <td class="grey-p">Diamond</td>
                            <td class="grey-p">:</td>
                            <td><?php echo $productDetails[$cnt]->getDiamond();?></td>
                        </tr>
                        <tr>
                            <td class="grey-p">Description</td>
                            <td class="grey-p">:</td>
                            <td><?php echo $productDetails[$cnt]->getDescription();?></td>
                        </tr>                                                                                                                                         
                    </table>
                </div>
            <?php
            }
            ?>
            <?php
            }
            ?>

        <?php
        }
        ?>
        <?php
        }
        ?>
    </div>

</div>

<div class="clear"></div>

<div class="sticky-distance2 width100 call-sticky-distance"></div>

<?php include 'js.php'; ?>
</body>
</html>