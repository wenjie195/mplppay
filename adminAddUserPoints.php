<?php
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';
$conn = connDB();

include 'selectFilecss.php';

require_once('vendor/php-excel-reader/excel_reader2.php');
require_once('vendor/SpreadsheetReader.php');

if (isset($_POST["import"]))
{
  $allowedFileType = ['application/vnd.ms-excel','text/xls','text/xlsx','application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'];
  if(in_array($_FILES["file"]["type"],$allowedFileType))
  {
    $targetPath = 'uploadsExcel/'.$timestamp.$_FILES['file']['name'];
    move_uploaded_file($_FILES['file']['tmp_name'], $targetPath);
    $Reader = new SpreadsheetReader($targetPath);
    $sheetCount = count($Reader->sheets());
    for($i=0;$i<$sheetCount;$i++)
    {
      $Reader->ChangeSheet($i);
      foreach ($Reader as $Row)
      {
        $userID = "";
        if(isset($Row[0])) 
        {
          $userID = mysqli_real_escape_string($conn,$Row[0]);
        }
        $userName = "";
        if(isset($Row[0])) 
        {
          $userName = mysqli_real_escape_string($conn,$Row[1]);
        }
        $point = "";
        if(isset($Row[1]))
        {
          $point = mysqli_real_escape_string($conn,$Row[2]);
        }
        if (!empty($userName) || !empty($point) || !empty($userID)) 
        {
          //$descriptionNew =  password_hash($description, PASSWORD_DEFAULT);
          $query = "UPDATE `users` SET `point`='".$point."' WHERE username = '".$userName."' OR user_id = '".$userID."' ";
          $result = mysqli_query($conn, $query);
          if (! empty($result))
          {
            echo "<script>alert('Excel file uploaded !!');window.location='../ppay10/adminAddUserPoints.php'</script>";
          }
          else
          {
            echo "<script>alert('Fail to upload excel file !!');window.location='../ppay10/adminAddUserPoints.php'</script>";
          }
        }
      }
    }
  }
  else
  {
    echo "<script>alert('Problem in Importing Excel Data !!');window.location='../ppay10/adminAddUserPoints.php'</script>";
  }
}
?>

<!DOCTYPE html>
<html>
<head>
  <?php include 'meta.php'; ?>
  <!-- <meta property="og:url" content="https://qlianmeng.asia/addReferee.php" /> -->
  <meta property="og:title" content="Admin Top Up User Points (Directly Renew) | PPay" />
  <title>Admin Top Up User Points (Directly Renew) | PPay</title>
  <!-- <link rel="canonical" href="https://qlianmeng.asia/addReferee.php" /> -->
  <?php include 'css.php'; ?>
</head>

<body class="body">

<?php include 'header.php'; ?>




  <div class="width100 same-padding menu-distance2 admin-min-height-with-distance text-center">
  	<h1 class="h1-title green-text"><?php echo _ADMIN_IMPORT_EXCEL ?> (<?php echo _ADMIN_RENEW_USER_POINTSS ?>)</h1>
    <form action="" method="post" name="frmExcelImport" id="frmExcelImport" enctype="multipart/form-data">
     <input type="file" name="file" id="file" class="choose-file-input" accept=".xls,.xlsx"><div class="clear"></div>
      <button type="submit" id="submit" name="import"  class="clean blue-gradient-button"><?php echo _USER_SUBMIT ?></button>
      <div id="response" class="<?php if(!empty($type)) { echo $type . " display-block"; } ?>"><?php if(!empty($message)) { echo $message; } ?></div>
    </form>
  </div>

<style>
.import-li{
color:#bf1b37;
background-color:white;}
.import-li .hover1a{
display:none;}
.import-li .hover1b{
display:block;}
</style>

<?php include 'js.php'; ?>

<script  src="https://code.jquery.com/jquery-2.2.0.min.js"></script>
<script type="text/javascript" src="js/modernizr.custom.js"></script>
<script type="text/javascript" src="js/jquery.dlmenu.js"></script>
		<script>
			$(function() {
				$( '#dl-menu' ).dlmenu({
					animationClasses : { classin : 'dl-animate-in-2', classout : 'dl-animate-out-2' }
				});
			});
    </script>
</body>
</html>