<?php
if (session_id() == "")
{
  session_start();
}
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';
// require_once dirname(__FILE__) . '/sessionLoginChecker.php';

require_once dirname(__FILE__) . '/classes/Product.php';
// require_once dirname(__FILE__) . '/classes/Variation.php';
require_once dirname(__FILE__) . '/classes/Slider.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$conn = connDB();

$slider = getSlider($conn," WHERE status = 'Show' ");
$products = getProduct($conn, "WHERE status = 'Available' ");
// $products = getProduct($conn, "WHERE featured_product = 'Yes' ORDER BY date_created DESC LIMIT 10 ");
// $variationDetails = getVariation($conn);

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<!--<meta property="og:url" content="https://thousandmedia.asia/" />-->
<!--<link rel="canonical" href="https://thousandmedia.asia/" />-->
<meta property="og:title" content="PPay" />
<title>PPay</title>
<meta property="og:description" content="PPay" />
<meta name="description" content="PPay" />
<meta name="keywords" content="PPay,e-commerce,iphone,phone,huawei">

 <link rel="stylesheet" type="text/css" href="css/glider.css">
<?php include 'css.php'; ?>
</head>

<body class="body">

<?php echo '<script type="text/javascript" src="js/jquery-3.3.1.min.js"></script>'; ?>

<?php include 'header.php'; ?>
<div class="min-height overflow width100">
  <div id="jssor_1" style="position:relative;left:0px;width:1300px;height:500px;overflow:hidden;visibility:hidden;" class="menu-distance">
    <!-- Loading Screen -->
    <div data-u="loading" class="jssorl-009-spin" style="position:absolute;top:0px;left:0px;width:100%;height:100%;text-align:center;background-color:rgba(0,0,0,0.7);"></div>
    <div data-u="slides" style="cursor:default;position:relative;top:0px;left:0px;width:1300px;height:500px;overflow:hidden;">
      <?php
      $conn = connDB();
      if($slider)
      {
        for($cnt = 0;$cnt < count($slider) ;$cnt++)
        {
        ?>
          <a href="#" target="_blank">
            <div>
              <img data-u="image" src="uploadsSlider/<?php echo $slider[$cnt]->getImgName();?>" class="pointer opacity-hover" />
            </div>
          </a>
        <?php
        }
        ?>
      <?php
      }
      $conn->close();
      ?>
    </div>

    <!-- Bullet Navigator -->
    <div data-u="navigator" class="jssorb032" style="position:absolute;bottom:12px;right:12px;" data-autocenter="1" data-scale="0.5" data-scale-bottom="0.75">
      <div data-u="prototype" class="i" style="width:16px;height:16px;">
        <svg viewBox="0 0 16000 16000" style="position:absolute;top:0;left:0;width:100%;height:100%;">
          <circle class="b" cx="8000" cy="8000" r="5800"></circle>
        </svg>
      </div>
    </div>

    <!-- Arrow Navigator -->
    <div data-u="arrowleft" class="jssora051" style="width:65px;height:65px;top:0px;left:25px;" data-autocenter="2" data-scale="0.75" data-scale-left="0.75">
      <svg viewBox="0 0 16000 16000" style="position:absolute;top:0;left:0;width:100%;height:100%;">
        <polyline class="a" points="11040,1920 4960,8000 11040,14080 "></polyline>
      </svg>
    </div>

    <div data-u="arrowright" class="jssora051" style="width:65px;height:65px;top:0px;right:25px;" data-autocenter="2" data-scale="0.75" data-scale-right="0.75">
      <svg viewBox="0 0 16000 16000" style="position:absolute;top:0;left:0;width:100%;height:100%;">
        <polyline class="a" points="4960,1920 11040,8000 4960,14080 "></polyline>
      </svg>
    </div>
  </div>

  <div class="clear"></div>

  <div class="width100 small-padding">
    <h1 class="user-title left-align-title slider-h1">Products</h1>
  </div>

  <div class="clear"></div>

  <div class="width100 small-padding glider-contain ow-product-gilder">
    <div class="glider glider3">
      <div class="shadow-white-box product-box opacity-hover">
        <div class="width100 white-bg image-box-size"> 
        <img src="img/product-1.jpg"  class="width100 two-border-radius">
        </div>
        
        <div class="width100 product-details-div">
        <p class="width100 text-overflow slider-product-name">Real Madrid (Home) Jersey</p>
        <p class="slider-product-name">Coming Soon</p>
        </div>
      </div>
      <div class="shadow-white-box product-box opacity-hover">
        <div class="width100 white-bg image-box-size"> 
          <img src="img/product-2.jpg" class="width100 two-border-radius">
        </div>

        <div class="width100 product-details-div">
          <p class="width100 text-overflow slider-product-name">Liverpool (Home) Jersey</p>
          <p class="slider-product-name">Coming Soon</p>
        </div>
      </div>
      <div class="shadow-white-box product-box opacity-hover">
        <div class="width100 white-bg image-box-size"> 
          <img src="img/product-3.jpg" class="width100 two-border-radius">
        </div>

        <div class="width100 product-details-div">
          <p class="width100 text-overflow slider-product-name">FC Barcelona (Home) Jersey</p>
          <p class="slider-product-name">Coming Soon</p>
        </div>
      </div>
      <div class="shadow-white-box product-box opacity-hover">
        <div class="width100 white-bg image-box-size"> 
          <img src="img/product-4.jpg" class="width100 two-border-radius">
        </div>

        <div class="width100 product-details-div">
          <p class="width100 text-overflow slider-product-name">Manchester United (Home) Jersey</p>
          <p class="slider-product-name">Coming Soon</p>
        </div>
      </div>              
      <div class="shadow-white-box product-box opacity-hover">
        <div class="width100 white-bg image-box-size"> 
          <img src="img/product-5.png" class="width100 two-border-radius">
        </div>

        <div class="width100 product-details-div">
          <p class="width100 text-overflow slider-product-name">FC Bayern Munchen (Home) Jersey</p>
          <p class="slider-product-name">Coming Soon</p>
        </div>
      </div>      
      <div class="shadow-white-box product-box opacity-hover">
        <div class="width100 white-bg image-box-size"> 
          <img src="img/product-6.jpg" class="width100 two-border-radius">
        </div>

        <div class="width100 product-details-div">
          <p class="width100 text-overflow slider-product-name">Play Station 4</p>
          <p class="slider-product-name">Coming Soon</p>
        </div>
      </div>                
      <div class="shadow-white-box product-box opacity-hover">
        <div class="width100 white-bg image-box-size"> 
          <img src="img/product-7.jpg" class="width100 two-border-radius">
        </div>

        <div class="width100 product-details-div">
          <p class="width100 text-overflow slider-product-name">Huawei P10</p>
          <p class="slider-product-name">Coming Soon</p>
        </div>
      </div>       
      <div class="shadow-white-box product-box opacity-hover">
        <div class="width100 white-bg image-box-size"> 
          <img src="img/product-8.jpg" class="width100 two-border-radius">
        </div>

        <div class="width100 product-details-div">
          <p class="width100 text-overflow slider-product-name">Samsung Note 10 Plus</p>
          <p class="slider-product-name">Coming Soon</p>
        </div>
      </div>                 
      <div class="shadow-white-box product-box opacity-hover">
        <div class="width100 white-bg image-box-size"> 
          <img src="img/product-9.jpg" class="width100 two-border-radius">
        </div>

        <div class="width100 product-details-div">
          <p class="width100 text-overflow slider-product-name">Apple Watch</p>
          <p class="slider-product-name">Coming Soon</p>
        </div>
      </div>                                  
    </div>
  </div>
</div>
<div class="clear"></div>

<style type="text/css">
* {
  box-sizing: border-box;
}
div{outline:none !important;}
.home-a .hover1a{
display:none !important;}
.home-a .hover1b{
display:inline-block !important;}
@media (max-width: 800px){
  .slick-slide {
  margin: 0px 8px;
  }		
}
</style>

<?php include 'js.php'; ?>

<script src="js/glider.js"></script>
<script>
  window.addEventListener('load',function(){
    document.querySelector('.glider1').addEventListener('glider-slide-visible', function(event){
        var glider = Glider(this);
        console.log('Slide Visible %s', event.detail.slide)
    });
    document.querySelector('.glider1').addEventListener('glider-slide-hidden', function(event){
        console.log('Slide Hidden %s', event.detail.slide)
    });
    document.querySelector('.glider1').addEventListener('glider-refresh', function(event){
        console.log('Refresh')
    });
    document.querySelector('.glider1').addEventListener('glider-loaded', function(event){
        console.log('Loaded')
    });

    window._ = new Glider(document.querySelector('.glider1'), {
        slidesToShow: 'auto', //'auto',
  slidesToScroll: 1,
        draggable: true,
        scrollLock: false,
        rewind: true,
    });
  });
</script>

<script>
  window.addEventListener('load',function(){
    document.querySelector('.glider2').addEventListener('glider-slide-visible', function(event){
        var glider = Glider(this);
        console.log('Slide Visible %s', event.detail.slide)
    });
    document.querySelector('.glider2').addEventListener('glider-slide-hidden', function(event){
        console.log('Slide Hidden %s', event.detail.slide)
    });
    document.querySelector('.glider2').addEventListener('glider-refresh', function(event){
        console.log('Refresh')
    });
    document.querySelector('.glider2').addEventListener('glider-loaded', function(event){
        console.log('Loaded')
    });

    window._ = new Glider(document.querySelector('.glider2'), {
        slidesToShow: 'auto', //'auto',
  slidesToScroll: 1,
        draggable: true,
        scrollLock: false,
        rewind: true,
    });
  });
</script>

<script>
  window.addEventListener('load',function(){
    document.querySelector('.glider3').addEventListener('glider-slide-visible', function(event){
        var glider = Glider(this);
        console.log('Slide Visible %s', event.detail.slide)
    });
    document.querySelector('.glider3').addEventListener('glider-slide-hidden', function(event){
        console.log('Slide Hidden %s', event.detail.slide)
    });
    document.querySelector('.glider3').addEventListener('glider-refresh', function(event){
        console.log('Refresh')
    });
    document.querySelector('.glider3').addEventListener('glider-loaded', function(event){
        console.log('Loaded')
    });

    window._ = new Glider(document.querySelector('.glider3'), {
        slidesToShow: 'auto', //'auto',
  slidesToScroll: 1,
        draggable: true,
        scrollLock: false,
        rewind: true,
    });
  });
</script>  

<?php
if(isset($_GET['type']))
{
    $messageType = null;

    if($_SESSION['messageType'] == 1)
    {
        if($_GET['type'] == 1)
        {
            $messageType = "Login Succesfully !!"; 
        }
        else if($_GET['type'] == 2)
        {
            $messageType = "Unknown User, Please contact admin !!";
        }
        else if($_GET['type'] == 3)
        {
            $messageType = "Wrong Password";
        }
        else if($_GET['type'] == 4)
        {
            $messageType = "Please register !!";
        }
        echo '
        <script>
            putNoticeJavascript("Notice !! ","'.$messageType.'");  
        </script>
        ';   
        $_SESSION['messageType'] = 0;
    }
}
?>

</body>
</html>