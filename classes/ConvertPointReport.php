<?php
class ConvertPointReport {
    /* Member variables */
    var $id,$uid,$userId,$userName,$currentPoints,$convertPoints,$currentDiamonds,$diamond,$dateCreated,$dateUpdated;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getUid()
    {
        return $this->uid;
    }

    /**
     * @param mixed $uid
     */
    public function setUid($uid)
    {
        $this->uid = $uid;
    }

    /**
     * @return mixed
     */
    public function getUserId()
    {
        return $this->userId;
    }

    /**
     * @param mixed $userId
     */
    public function setUserId($userId)
    {
        $this->userId = $userId;
    }

    /**
     * @return mixed
     */
    public function getUsername()
    {
        return $this->userName;
    }

    /**
     * @param mixed $userName
     */
    public function setUsername($userName)
    {
        $this->userName = $userName;
    }

    /**
     * @return mixed
     */
    public function getCurrentPoints()
    {
        return $this->currentPoints;
    }

    /**
     * @param mixed $currentPoints
     */
    public function setCurrentPoints($currentPoints)
    {
        $this->currentPoints = $currentPoints;
    }

    /**
     * @return mixed
     */
    public function getConvertPoints()
    {
        return $this->convertPoints;
    }

    /**
     * @param mixed $convertPoints
     */
    public function setConvertPoints($convertPoints)
    {
        $this->convertPoints = $convertPoints;
    }

    /**
     * @return mixed
     */
    public function getCurrentDiamond()
    {
        return $this->currentDiamonds;
    }

    /**
     * @param mixed $currentDiamonds
     */
    public function setCurrentDiamond($currentDiamonds)
    {
        $this->currentDiamonds = $currentDiamonds;
    }

    /**
     * @return mixed
     */
    public function getDiamond()
    {
        return $this->diamond;
    }

    /**
     * @param mixed $diamond
     */
    public function setDiamond($diamond)
    {
        $this->diamond = $diamond;
    }

    /**
     * @return mixed
     */
    public function getDateCreated()
    {
        return $this->dateCreated;
    }

    /**
     * @param mixed $dateCreated
     */
    public function setDateCreated($dateCreated)
    {
        $this->dateCreated = $dateCreated;
    }

    /**
     * @return mixed
     */
    public function getDateUpdated()
    {
        return $this->dateUpdated;
    }

    /**
     * @param mixed $dateUpdated
     */
    public function setDateUpdated($dateUpdated)
    {
        $this->dateUpdated = $dateUpdated;
    }

}

function getConvertPointReport($conn,$whereClause = null,$queryColumns = null,$queryValues = null,$queryTypes = null){
    $dbColumnNames = array("id","uid","user_id","user_name","current_points","convert_points","current_diamond","diamond","date_created","date_updated");

    $sql = sqlSelectSimpleBuilder($dbColumnNames,"convert_point");
    if($whereClause){
        $sql .= $whereClause;
    }

    if($stmt = $conn->prepare($sql)){
        /*
             Binds variables to prepared statement

             i    corresponding variable has type integer
             d    corresponding variable has type double
             s    corresponding variable has type string
             b    corresponding variable is a blob and will be sent in packets
        */

        if($queryColumns&&$queryTypes&&$queryValues){
            $stmt = returnStmtWithDynamicBinding($stmt,$queryValues,$queryTypes);
        }

//        $stmt->bind_param('s',$queryValues[0]);

        /* execute query */
        $stmt->execute();

        /* Store the result (to get properties) */
        $stmt->store_result();

        /* Get the number of rows */
        $num_of_rows = $stmt->num_rows;

        /* Bind the result to variables */
        $stmt->bind_result($id,$uid,$userId,$userName,$currentPoints,$convertPoints,$currentDiamonds,$diamond,$dateCreated,$dateUpdated);

        $resultRows = array();
        while ($stmt->fetch()) {
            $class = new ConvertPointReport();
            $class->setId($id);
            $class->setUid($uid);
            $class->setUserId($userId);
            $class->setUsername($userName);
            $class->setCurrentPoints($currentPoints);
            $class->setConvertPoints($convertPoints);
            $class->setCurrentDiamond($currentDiamonds);
            $class->setDiamond($diamond);
            $class->setDateCreated($dateCreated);
            $class->setDateUpdated($dateUpdated);

            array_push($resultRows,$class);
        }

        /* free results */
        $stmt->free_result();

        /* close statement */
        $stmt->close();

        if($num_of_rows <= 0){
            return null;
        }else{
            return $resultRows;
        }
    }else{
//        echo "Prepare Error: ($conn->errno) $conn->error";
        return null;
    }

}