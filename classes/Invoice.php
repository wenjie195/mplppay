<?php  
class Product {
    /* Member variables */
    var $id,$uid,$category,$brand,$name,$sku,$slug,$price,$diamond,$status,$description,$keywordOne,$link,$imageOne,$dateCreated,$dateUpdated;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getUid()
    {
        return $this->uid;
    }

    /**
     * @param mixed $uid
     */
    public function setUid($uid)
    {
        $this->uid = $uid;
    }

    /**
     * @return mixed
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * @param mixed $category
     */
    public function setCategory($category)
    {
        $this->category = $category;
    }

    /**
     * @return mixed
     */
    public function getBrand()
    {
        return $this->brand;
    }

    /**
     * @param mixed $brand
     */
    public function setBrand($brand)
    {
        $this->brand = $brand;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getSku()
    {
        return $this->sku;
    }

    /**
     * @param mixed $sku
     */
    public function setSku($sku)
    {
        $this->sku = $sku;
    }

    /**
     * @return mixed 
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * @param mixed $slug
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;
    }

    /**
     * @return mixed
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * @param mixed $price
     */
    public function setPrice($price)
    {
        $this->price = $price;
    }

    /**
     * @return mixed
     */
    public function getDiamond()
    {
        return $this->diamond;
    }

    /**
     * @param mixed $diamond
     */
    public function setDiamond($diamond)
    {
        $this->diamond = $diamond;
    }

    /**
     * @return mixed
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param mixed $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

    /**
     * @return mixed
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param mixed $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * @return mixed
     */
    public function getKeywordOne()
    {
        return $this->keywordOne;
    }

    /**
     * @param mixed $keywordOne
     */
    public function setKeywordOne($keywordOne)
    {
        $this->keywordOne = $keywordOne;
    }

    /**
     * @return mixed
     */
    public function getLink()
    {
        return $this->link;
    }

    /**
     * @param mixed $link
     */
    public function setLink($link)
    {
        $this->link = $link;
    }

    /**
     * @return mixed
     */
    public function getImageOne()
    {
        return $this->imageOne;
    }

    /**
     * @param mixed $imageOne
     */
    public function setImageOne($imageOne)
    {
        $this->imageOne = $imageOne;
    }

    /**
     * @return mixed
     */
    public function getDateCreated()
    {
        return $this->dateCreated;
    }

    /**
     * @param mixed $dateCreated
     */
    public function setDateCreated($dateCreated)
    {
        $this->dateCreated = $dateCreated;
    }

    /**
     * @return mixed
     */
    public function getDateUpdated()
    {
        return $this->dateUpdated;
    }

    /**
     * @param mixed $dateUpdated
     */
    public function setDateUpdated($dateUpdated)
    {
        $this->dateUpdated = $dateUpdated;
    }

}

function getProduct($conn,$whereClause = null,$queryColumns = null,$queryValues = null,$queryTypes = null){
    $dbColumnNames = array("id","uid","category","brand","name","sku","slug","price","diamond","status","description","keyword_one","link","image_one",
                            "date_created","date_updated");

    $sql = sqlSelectSimpleBuilder($dbColumnNames,"product");
    if($whereClause){
        $sql .= $whereClause;
    }

    if($stmt = $conn->prepare($sql)){
        /*
             Binds variables to prepared statement

             i    corresponding variable has type integer
             d    corresponding variable has type double
             s    corresponding variable has type string
             b    corresponding variable is a blob and will be sent in packets
        */

        if($queryColumns&&$queryTypes&&$queryValues){
            $stmt = returnStmtWithDynamicBinding($stmt,$queryValues,$queryTypes);
        }

//        $stmt->bind_param('s',$queryValues[0]);

        /* execute query */
        $stmt->execute();

        /* Store the result (to get properties) */
        $stmt->store_result();

        /* Get the number of rows */
        $num_of_rows = $stmt->num_rows;

        /* Bind the result to variables */
        $stmt->bind_result($id,$uid,$category,$brand,$name,$sku,$slug,$price,$diamond,$status,$description,$keywordOne,$link,$imageOne,$dateCreated,$dateUpdated);

        $resultRows = array();
        while ($stmt->fetch()) {
            $class = new Product;
            $class->setId($id);
            $class->setUid($uid);
            $class->setCategory($category);
            $class->setBrand($brand);
            $class->setName($name);
            $class->setSku($sku);
            $class->setSlug($slug);
            $class->setPrice($price);
            $class->setDiamond($diamond);
            $class->setStatus($status);
            $class->setDescription($description);
            $class->setKeywordOne($keywordOne);
            $class->setLink($link);
            $class->setImageOne($imageOne);
            $class->setDateCreated($dateCreated);
            $class->setDateUpdated($dateUpdated);
          
            array_push($resultRows,$class);
        }

        /* free results */
        $stmt->free_result();

        /* close statement */
        $stmt->close();

        if($num_of_rows <= 0){
            return null;
        }else{
            return $resultRows;
        }
    }else{
//        echo "Prepare Error: ($conn->errno) $conn->error";
        return null;
    }
}

function createProductList($products,$cartType = 1,$postQuantityRows = null,$isIncludeNotSelectedProductToo = true){
    /*
     * CART TYPE
     * 1 = normal product display cart
     * 2 = checkout cart
     */

    $productListHtml = "";

    if(!$products){
        return $productListHtml;
    }
    //$CumPrice = 0;
    $subtotal = 0;
    $shippingfees = 0;
    $totalA = 0;
    $total = 0;
    $index = 0;

    foreach ($products as $product){
        $quantity = 0;
        if($postQuantityRows){
            $quantity = $postQuantityRows[$index];
        }

        $totalPrice = "";


        if($quantity <= 0 && !$isIncludeNotSelectedProductToo){

            $productListHtml .= '<div style="display: none;">';

        }else{
            $totalPrice = $quantity * $product->getDiamond();;
            // $CumPrice += $totalPrice;
            $subtotal += $totalPrice;
            $totalA = $subtotal;
            if($totalA < 100){
                $shippingfees = 30;
                $total = $totalA + 30;
            }else{
                $shippingfees = 0;
                $total = $totalA + 0;
            }
            $_SESSION['total'] = $total;

            $productListHtml .= '<div style="display: block;">';

        }

        $conn=connDB();
            //$productArray = getProduct($conn);
            $id  = $product->getName();
            // Include the database configuration file


            // Get images from the database
            $query = $conn->query("SELECT image_one FROM product WHERE name = '$id'");

            if($query->num_rows > 0){
                while($row = $query->fetch_assoc()){
                    $imageURL = './uploads/'.$row["image_one"];

        $productListHtml .= '

            </div>
        ';
                    }
        }
        $index++;

    }

    //$productListHtml .= '<h2 class="product-name-h2">Subtotal : ' .$CumPrice.' Points<h2>';
    $productListHtml .=
    '
    <div class="cart-bottom-div">
                <div class="left-cart-bottom-div">
                    <p class="continue-shopping pointer continue2">
                        
                    </p>
                </div>
            </div>
                <div class="clear"></div>
                <div class="no-sticky-bottom">
                        <div class="grey-border"></div>
                        <p class="left-bottom-price">Sub-total</p>
                        <p class="right-bottom-price">RM '.$subtotal.'.00</p>
                        <input type="hidden" class="right-bottom-price" value="'.$subtotal.'" name="subtotal" id="subtotal" >
                        <div class="clear"></div>
                        <p class="left-bottom-price">Shipping</p>
                        <p class="right-bottom-price"> RM' .$shippingfees.'.00</p>  
                        <div class="clear"></div>            
                </div>
                <div class="sticky-bottom-price same-padding3">
                        <div class="grey-border"></div>
                        <p class="left-bottom-price weight900">Total</p>
                        <p class="right-bottom-price weight900">RM' .$total.'.00</p>   
                        <input type="hidden" class="right-bottom-price weight900" value="'.$total.'" name="total" id="total">
                        <div class="clear"></div>  
                        <div class="width100 text-center">                                                                         
                            <button class="green-button checkout-btn clean" id="completeOrder">COMPLETE ORDER</button>
                        </div>
                </div>
                <div class="sticky-distance-bottom"></div>       
            </div>
            </br></br>

    ';

    return $productListHtml;
}

function getProductPrice($conn,$productId){
    $price = 0;

    $productRows = getProduct($conn," WHERE id = ? ",array("id"),array($productId),"i");

    if($productRows){
        $price = $productRows[0]->getDaimond();
    }

    return $price;
}

function getProductName($conn,$productId){
    $name = "";

    $productRows = getProduct($conn," WHERE id = ? ",array("id"),array($productId),"i");

    if($productRows){
        $name = $productRows[0]->getName();
    }

    return $name;
}

function addToCart(){
    $shoppingCart = array();

    $totalProductCount = count($_POST['product-list-id-input']);
    for($i = 0; $i < $totalProductCount; $i++){
        $productId = $_POST['product-list-id-input'][$i];
        $quantity = $_POST['product-list-quantity-input'][$i];

//        if($quantity > 0){
        $thisOrder = array();
        $thisOrder['productId'] = $productId;
        $thisOrder['quantity'] = $quantity;
        array_push($shoppingCart,$thisOrder);
//        }
    }

    if(count($shoppingCart) > 0) {
        $_SESSION['shoppingCart'] = $shoppingCart;
    }

}

function clearCart(){
    unset ($_SESSION["shoppingCart"]);
}

//when checkout only create order
function createOrder($conn,$uid){
    if(isset($_SESSION['shoppingCart']) && $_SESSION['shoppingCart'] && count($_SESSION['shoppingCart']) > 0){
        $shoppingCart = $_SESSION['shoppingCart'];
        $order_Id = md5(uniqid());

        $orderId = insertDynamicData($conn,"orders",array("uid","order_id"),array($uid,$order_Id),"ss");

        if($orderId){
            $totalPrice = 0;


            for($index = 0; $index < count($shoppingCart); $index++){
                $thisCart = $shoppingCart[$index];
                $productId = $thisCart['productId'];
                // $_SESSION['productName'] = $productId;
                $quantity = $thisCart['quantity'];
                $originalPrice = getProductPrice($conn,$productId);
                $productName = getProductName($conn,$productId);
                $totalPrice += ($originalPrice * $product->getDiamond());

                if($quantity <= 0){
                    continue;
                }

                if(!insertDynamicData($conn,"product_orders",array("product_id","product_name","order_id","quantity","final_price","original_price","discount_given","totalProductPrice"),
                    array($productId,$productName,$orderId,$quantity,$originalPrice,$originalPrice,0,$totalPrice),"isiidddd")){
                    promptError("error creating order for product : $productId");
                }
            }

        }else{
            promptError("error creating order");
        }

    }
}

function getShoppingCart($conn,$cartType = 2,$isIncludeNotSelectedProductToo = false){
    $productListHtml = "";

    if(isset($_SESSION['shoppingCart']) && $_SESSION['shoppingCart']){
        $products = array();
        $quantities = array();
        for($index = 0; $index < count($_SESSION['shoppingCart']); $index++){
            $thisCart = $_SESSION['shoppingCart'][$index];

            $tempProductRows = getProduct($conn," WHERE id = ? ",array("id"),array($thisCart['productId']),"i");
            if($tempProductRows){
                array_push($products,$tempProductRows[0]);
                array_push($quantities,$thisCart['quantity']);
            }
        }

        if(count($products) > 0 && count($quantities) > 0 && count($products) == count($quantities)){
            $productListHtml = createProductList($products,$cartType,$quantities,$isIncludeNotSelectedProductToo);
        }
    }

    return $productListHtml;
}